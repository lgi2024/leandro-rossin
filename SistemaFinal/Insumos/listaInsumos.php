<?php
include '../Principal/pagPrincipal.php';
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';

//Acá recibimos las opciones de busqueda
$orden = isset($_POST['ordenarPor']) ? $_POST['ordenarPor'] : "idasc"; // Valor por defecto para ordenar por ID
$direccion = "ASC"; // Orden ascendente por defecto
?>



<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Insumos</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListas.css">
    </head>
    <body>
        <div class="contenedor">
            <h1>Lista de Insumos</h1>
                <!--Buscar datos-->
                <div class="busqueda">
                    <label>Búsqueda:</label> <input type="text" id="busqueda" name="busqueda" 
                    onkeyup="buscarInsumos()" placeholder="Buscar...">
                </div>

                <!--Metodos de ordenamiento de la lista-->
                <form action="" method="post">
                    <label for="ordenarPor">Ordenar por:</label>
                    <select name="ordenarPor" id="ordenarPor">
                        <option value="idasc" <?php if ($orden == "idasc") echo 'selected';?>>Id (asc)</option>
                        <option value="iddesc" <?php if ($orden == "iddesc") echo 'selected';?>>Id (desc)</option>
                        <option value="nombreasc" <?php if ($orden =="nombreasc") echo 'selected'; ?>>Nombre (asc)</option>
                        <option value="nombredesc" <?php if ($orden == "nombredesc") echo 'selected'; ?>>Nombre (desc)</option>
                        <option value="provasc" <?php if($orden == "provasc") echo  'selected'; ?>>Proveedor (asc)</option>
                        <option value="provdesc" <?php if($orden == "provdesc") echo  'selected'; ?>>Proveedor (desc)</option>
                        <option value="fechaasc" <?php if($orden == "fechaasc") echo  'selected'; ?>>Fecha (asc)</option>
                        <option value="fechadesc" <?php if($orden == "fechadesc") echo  'selected'; ?>>Fecha (desc)</option>
                    </select>
                    <input type="submit" value="Ordenar">
                </form>

            <form action="agregarInsumo.php" id=agregar>
                <button type="submit" id="btnAgregar">+ Nuevo Insumo</button>
            </form>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $orden = $_POST['ordenarPor'];

    if ($orden == 'idasc' || $orden == 'iddesc')
        $campoOrden = 'Id';
    elseif ($orden == 'nombreasc' || $orden == 'nombredesc') 
        $campoOrden = 'Nombre';
    elseif ($orden == 'provasc' || $orden == 'provdesc') 
        $campoOrden = 'proveedor';
    elseif ($orden == 'fechaasc' || $orden == 'fechadesc') 
        $campoOrden = 'Fecha';

    $direccion = ($orden == 'nombreasc' || $orden == 'idasc' || $orden == 'provasc' || $orden == 'fechaasc') ? 'ASC' : 'DESC';
} else {
    $campoOrden = 'id'; //Si recien entramos a la ventana, veremos la lista ordenada por el id
}

//Con esta consulta armamos nuestra tabla
$sql = "SELECT Foto, insumos.Id, insumos.Nombre, PrecioUnitario, Stock, proveedores.Nombre AS proveedor, Fecha FROM insumos INNER JOIN 
    proveedores ON proveedores.Id=insumos.IdProveedor ORDER BY $campoOrden $direccion";
$result = mysqli_query($con, $sql);


//Mostramos los resultados de la consulta
if (mysqli_num_rows($result) > 0) {
    echo "<table id=tabla border='1'><tr> 
    <th>Foto</th>
    <th>Id</th> 
    <th>Nombre</th> 
    <th>Precio Unitario</th> 
    <th>Stock</th>
    <th>Proveedor</th>
    <th>Fecha</th>";
    if ($_SESSION['Rol'] == 'Administrador')
        echo "<th colspan='2'>Operaciones</th></tr>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td><img src='" . $row["Foto"] . "' alt='Foto' width='50'></td>
                <td>" . $row["Id"] . "</td>
                <td>" . $row["Nombre"] . "</td>
                <td>" . $row["PrecioUnitario"] . "</td>
                <td>" . $row["Stock"] . "</td>
                <td>" . $row["proveedor"] . "</td>
                <td>" . $row["Fecha"] . "</td>"; 
                if ($_SESSION['Rol'] == 'Administrador') {
                    echo 
                    "<td><form action='modificarInsumo.php' method='GET'>
                        <input type='hidden' name='mod' value='" . $row['Id'] . "'>
                        <button type='submit' id='btnModificar'>Modificar</button>
                    </form></td>";
                    echo
                    "<td><form id='formEliminarIn" . $row['Id'] . "' action='eliminarInsumo.php' method='POST'>
                        <input type='hidden' name='elim' value='" . $row['Id'] . "'>
                        <button type='button' id='btnEliminar' onclick='confirmarEliminacionInsumo(" . $row['Id'] . ")'>Eliminar</button>
                    </form></td></tr>";
                }
    }
    echo "</table><br>";
} else {
    echo "0 resultados";
}
mysqli_close($con);
?>

</div>


    <!--Scripts-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../Scripts/buscarInsumos.js"></script>
    <script src="../Scripts/eliminar.js"></script>
</body>
</html>