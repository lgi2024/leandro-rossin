<?php
include '../Conexion/conexionBD.php';
include '../Principal/manejoImagen.php';
include '../Inicio/validarSesion.php';
?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Insumos</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estAgregar.css">
    </head>
    <body>
    <div class="container">
        <h1>ESENCAP</h1>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<!--Crear nuevo insumo-->
<form action="" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Añadir Nuevo Insumo</legend>
        Nombre: <input type="text" name="nombreins" required><br>
        Precio Unitario: <input type="number" name="precioins" required><br>
        Stock: <input type="number" name="stockins" required><br>
        Proveedor: 
        <?php
            $sql = "SELECT Id, Nombre FROM proveedores";
            $resultado = mysqli_query($con, $sql);
                if (mysqli_num_rows($resultado) > 0) {
                    echo "<select id='insumo' name='insumo'>";
                    while ($row = mysqli_fetch_assoc($resultado)) {
                        echo "<option value='" . $row['Id'] . "'>" . $row['Nombre'] . "</option>";
                    }
                    echo "</select><br>";
                }
        ?>
        Seleccionar Imagen: <input type="file" name="fotoin" id="fotoin"><br>
    
    <div class="cancelar">
        <input type="submit" value="Agregar Insumo">
        <input class="volver" type="button" value="Cancelar" onclick="window.history.back();">
    </div>
    </fieldset>
</form>


<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombreins = $_POST['nombreins'];
    $precioins = $_POST['precioins'];
    $stockins = $_POST['stockins'];
    $prov = $_POST['prov'];
    $target_file = "NULL";

    if (!empty($_FILES["foto"]["name"])) {
        $uploadResult = uploadImage($_FILES["foto"]);
        if ($uploadResult['status'] == 1) {
            $target_file = $uploadResult['path'];
        } else {
            echo $uploadResult['message'];
            exit();
        }
    }

    if($precioins >= 0){
        if($stockins > 0){
            $sql = "INSERT INTO insumos (Foto, Nombre, PrecioUnitario, Stock, IdProveedor) 
            VALUES ('$target_file', '$nombreins', '$precioins', '$stockins', '$prov')";

            if (mysqli_query($con, $sql)){
                echo "<script>
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Nuevo insumo agregado con exito',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    </script>";
                echo "<script>setTimeout(function(){ window.location.href = 'agregarInsumo.php'; }, 2000);</script>";
                exit();
            }
            else 
                echo "<script>
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Hubo un problema al agregar el producto. Inténtelo de nuevo.',
                    showConfirmButton: false,
                    timer: 3000
                });
                </script>";   
        } else 
            echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El stock debe ser mayor a 0. Intentelo de nuevo',
                showConfirmButton: false,
                timer: 3000
            });
            </script>";
    } else 
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'El precio debe ser mayor o igual a 0. Intentelo de nuevo',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
}
?>

</div>
</body>
</html>