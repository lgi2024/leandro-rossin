<?php
include '../Principal/pagPrincipal.php';
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';

$orden = isset($_POST['ordenarPor']) ? $_POST['ordenarPor'] : "idasc"; // Valor por defecto para ordenar por ID
$direccion = "ASC"; // Orden ascendente por defecto
?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Proveedores</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListas.css">
    </head>
    <body>
        <div class="contenedor">
            <h1>Lista de Proveedores</h1>
                <!--Buscar datos-->
                <div class="busqueda">
                    <label>Búsqueda:</label> <input type="text" id="busqueda" name="busqueda" 
                    onkeyup="buscarProv()" placeholder="Buscar...">
                </div>

                <!--Metodos de ordenamiento de la lista-->
                <form action="" method="POST">
                    <label>Ordenar por:</label>
                    <select name="ordenarPor" id="ordenarPor">
                        <option value="idasc" <?php if ($orden == "idasc") echo 'selected';?>>Id (asc)</option>
                        <option value="iddesc" <?php if ($orden == "iddesc") echo 'selected';?>>Id (desc)</option>
                        <option value="nombreasc" <?php if ($orden =='nombreasc') echo 'selected'; ?>>Nombre (asc)</option>
                        <option value="nombredesc" <?php if ($orden == 'nombredesc') echo 'selected'; ?>>Nombre (desc)</option>
                    </select>
                    <input type="submit" value="Ordenar">
                </form>
            
            <form action="agregarProv.php" id=agregar>
                <button type="submit" id="btnAgregar">+ Nuevo Proveedor</button>
            </form>




<?php
//Hacemos la consulta
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $orden = $_POST['ordenarPor'];

    if ($orden == 'idasc' || $orden == 'iddesc')
        $campoOrden = 'Id';
    elseif ($orden == 'nombreasc' || $orden == 'nombredesc') 
        $campoOrden = 'Nombre';

    $direccion = ($orden == 'nombreasc' || $orden == 'idasc') ? 'ASC' : 'DESC';
} else 
    $campoOrden = 'id'; /*Cuando entramos a la ventana, al inicio no seleccionamos ninguna forma para ordenar
    la lista, entonces por defecto se ordenará por el id*/

$sql = "SELECT Id, Nombre, Telefono FROM proveedores ORDER BY $campoOrden $direccion";
$result = mysqli_query($con, $sql);




//Mostramos los resultados de la consulta
if (mysqli_num_rows($result) > 0) {
    echo "<table id=tabla border='1'><tr> 
    <th>Id</th> 
    <th>Nombre</th> 
    <th>Telefono</th>
    <th colspan='2'>Operaciones</th></tr>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>" . $row["Id"] . "</td>
            <td>" . $row["Nombre"] . "</td>
            <td>" . $row["Telefono"] . "</td>";  
            echo 
            "<td><form action='modificarProv.php' method='GET'>
                <input type='hidden' name='mod' value='" . $row['Id'] . "'>
                <button type='submit' id='btnModificar'>Modificar</button>
            </form></td>";
            echo 
            "<td><form id='formEliminarp" . $row['Id'] . "' action='eliminarProv.php' method='POST'>
                <input type='hidden' name='elimp' value='" . $row['Id'] . "'>
                <button type='button' id='btnEliminar' onclick='confirmarEliminacionProv(" . $row['Id'] . ")'>Eliminar</button>
            </form></td></tr>";
    }
    echo "</table><br>";
} else 
    echo "No hay resultados";
?>

</div>

    <script src="../Scripts/buscarProv.js"></script>
    <script src="../Scripts/eliminar.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>
</html>