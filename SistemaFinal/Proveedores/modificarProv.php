<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Modificar proveedor</title>
    </head>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>

<?php
include '../Inicio/validarSesion.php';
include '../Conexion/conexionBD.php';
include '../Principal/manejoImagen.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $telefono = $_POST['telefono'];

    $sql = "UPDATE proveedores SET Nombre='$nombre', Telefono='$telefono'";
            
    // Ejecutar consulta
    if (mysqli_query($con, $sql)) 
        echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: 'Datos actualizados con éxito',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    else 
        echo "Error: " . $sql . "<br>" . mysqli_error($con);
}

/////////////////////////////////////////////////////////////////////

//Recibimos el id
$id = $_GET['mod'];

// Obtener los datos del producto
$sql = "SELECT Id, Nombre, Telefono FROM proveedores WHERE Id=$id";
$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $nombre = $row['Nombre'];
    $telefono = $row['Telefono'];
} else {
    echo "No se encontró al proveedor con ID: $id";
    exit();
}
mysqli_close($con);
?>

<form method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    Nombre: <input type="text" name="nombre" value="<?php echo $nombre; ?>" required><br>
    Telefono: <input type="tel" name="telefono" value="<?php echo $telefono; ?>" required><br>
    <input type="submit" value="Actualizar">
    <a href='listaProveedores.php'> Volver atrás </a>
</form>

