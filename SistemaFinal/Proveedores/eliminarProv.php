<html>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>
</html>

<?php
include '../Inicio/validarSesion.php';
include '../Conexion/conexionBD.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['elimp'])){
        $id = $_POST['elimp'];

        $sql = "DELETE FROM proveedores WHERE Id=$id";
        if (mysqli_query($con, $sql)) 
            echo "<script>
                    Swal.fire({
                        title: 'Proveedor eliminado correctamente',
                        icon: 'success'
                    }).then(() => {
                        window.location.href = 'listaProveedores.php';
                    });
                </script>";
        else
            echo "<script>
                Swal.fire({
                    title: 'Error al eliminar al proveedor',
                    icon: 'error'
                });
            </script>";
    }
}