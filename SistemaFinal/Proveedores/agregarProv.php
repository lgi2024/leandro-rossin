<?php
include '../Conexion/conexionBD.php';
include '../Principal/manejoImagen.php';
include '../Inicio/validarSesion.php';
?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Proveedores</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estAgregar.css">
    </head>
    <body>
    <div class="container">
        <h1>ESENCAP</h1>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>



<form action="" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Agregar Nuevo Proveedor</legend>
        <input type="hidden" name="tipo" value="prov">
        Nombre: <input type="text" name="nombre" required><br>
        Telefono: <input type="tel" name="telefono" required><br>
    <div class="cancelar">
        <input type="submit" value="Agregar Proveedor">
        <input class="volver" type="button" value="Cancelar" onclick="window.history.back();">
    </div>
    </fieldset>
</form>



<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $tipoEnvio = $_POST['tipo'];

    if ($tipoEnvio == "prov"){
        $nombreprov = $_POST['nombre'];
        $telefono = $_POST['telefono'];

        $sql = "INSERT INTO proveedores (Nombre, Telefono) VALUES ('$nombreprov', '$telefono')";

        if (mysqli_query($con, $sql)){
            echo "<script>
                Swal.fire({
                    icon: 'success',
                    title: 'Éxito',
                    text: 'Nuevo proveedor agregado con exito',
                    showConfirmButton: false,
                    timer: 3000
                });
                </script>";
            echo "<script>setTimeout(function(){ window.location.href = 'agregarProv.php'; }, 2000);</script>";
            exit();
        }else 
            echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Hubo un problema al agregar el proveedor. Inténtelo de nuevo.',
                showConfirmButton: false,
                timer: 3000
            });
            </script>";   
    }
}
?>

</div>
</body>
</html>