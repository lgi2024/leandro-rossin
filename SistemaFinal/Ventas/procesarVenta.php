<?php
include '../Conexion/conexionBD.php';
session_start();

if($_SERVER["REQUEST_METHOD"]=="POST"){
    if(isset($_POST['mostrarProductos']) && isset($_POST['cant'])){
        $idProducto = $_POST['mostrarProductos']; //mostrarProductos contiene el id del producto seleccionado
        $cantVenta = $_POST['cant'];

        //Consulta para obtener el stock disponible
        $sql = "SELECT Nombre, PrecioCosto, PrecioUnitario, Stock FROM productos WHERE Id=$idProducto";
        $result_agregar = mysqli_query($con, $sql);
        
        if (mysqli_num_rows($result_agregar) > 0){
            $fila = mysqli_fetch_assoc($result_agregar);
            $nombreProducto = $fila['Nombre'];
            $precioCosto = $fila['PrecioCosto'];
            $precioUnitario = $fila['PrecioUnitario'];
            $stockDisponible = $fila['Stock'];

            if($cantVenta>0){
                if($cantVenta > $stockDisponible)
                    $_SESSION['mensaje'] = 'error';
                else{
                    //Acá restamos al stock la cantidad que agregamos al carrito
                    $stockDisponible = $stockDisponible - $cantVenta;
                    $sql = "UPDATE productos SET Stock='$stockDisponible' WHERE Id=$idProducto";
                
                    if(mysqli_query($con, $sql)){
                        $_SESSION['mensaje'] = 'exito';

                        $productoCarrito = [
                            'id' => $idProducto,
                            'nombre' => $nombreProducto,
                            'cantidad' => $cantVenta,
                            'pCosto' => ($precioUnitario - $precioCosto) * $cantVenta,
                            'precio' => $precioUnitario,
                            'total' => $precioUnitario * $cantVenta
                        ];

                        // Si el carrito ya existe, agregar el nuevo producto
                        if (isset($_SESSION['carrito'])) 
                            $_SESSION['carrito'][] = $productoCarrito;
                        else 
                            $_SESSION['carrito'] = [$productoCarrito];
                    } 
                }
            }else
                $_SESSION['mensaje'] = 'invalido';
        }
        mysqli_close($con);

        // Redirigir a la página principal
        header("Location: ../Productos/listaProductos.php");
        exit();
    }
}
?>