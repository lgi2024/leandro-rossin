<?php
include '../Principal/pagPrincipal.php';
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';

$orden = isset($_POST['ordenarPor']) ? $_POST['ordenarPor'] : "codasc";
$direccion = "ASC";
?>



<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ventas</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListas.css">
    </head>
    <body>
        <div class="contenedor">
            <h1>Historial de Ventas</h1>

            <div class="busqueda">
                <label>Búsqueda:</label> <input type="text" id="busqueda" name="busqueda" 
                    onkeyup="buscarVenta()" placeholder="Buscar...">
            </div>

            <!--Metodos de ordenamiento de la lista-->
            <form action="" method="POST">
                <label>Ordenar por:</label>
                <select name="ordenarPor" id="ordenarPor">
                    <option value="codasc" <?php if ($orden == "codasc") echo 'selected';?>>Código Venta (asc)</option>
                    <option value="codesc" <?php if ($orden == "codesc") echo 'selected';?>>Código Venta (desc)</option>
                    <option value="prodasc" <?php if ($orden =='prodasc') echo 'selected'; ?>>Producto (asc)</option>
                    <option value="prodesc" <?php if ($orden == 'prodesc') echo 'selected'; ?>>Producto (desc)</option>
                    <option value="fechaasc" <?php if ($orden =='fechaasc') echo 'selected'; ?>>Fecha (asc)</option>
                    <option value="fechadesc" <?php if ($orden == 'fechadesc') echo 'selected'; ?>>Fecha (desc)</option>
                </select>
                <input type="submit" value="Ordenar">
            </form>

            <!--Botones-->
            <form action='../Graficos/graficos1.php'>
                <input type='submit' value='Gráficos'>
            </form>
            <?php if($_SESSION['Rol'] == 'Vendedor'): ?>
                <form action='../Productos/listaProductos.php'>
                <input type='submit' value='Volver atrás'>
                </form>
            <?php endif; ?>



<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $orden = $_POST['ordenarPor'];

    if ($orden == 'codasc' || $orden == 'codesc')
        $campoOrden = 'Cod';
    elseif ($orden == 'prodasc' || $orden == 'prodesc') 
        $campoOrden = 'Producto';
    elseif ($orden == 'fechaasc' || $orden == 'fechadesc') 
        $campoOrden = 'Fecha';


    $direccion = ($orden == 'prodasc' || $orden == 'codasc' || $orden == 'fechaasc') ? 'ASC' : 'DESC';
} else 
    $campoOrden = 'Cod';


$sql = "SELECT Cod, Id_producto, Producto, Cantidad, Ganancia, Fecha, Hora FROM ventas ORDER BY $campoOrden $direccion";
$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {
    echo "<table id=tabla border='1'><tr> 
    <th>Cod</th> 
    <th>Código producto</th>
    <th>Producto</th> 
    <th>Cantidad</th>
    <th>Ganancia</th>
    <th>Fecha</th>
    <th>Hora</th>";

    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>" . $row["Cod"] . "</td>
                <td>" . $row["Id_producto"] . "</td>
                <td>" . $row["Producto"] . "</td>
                <td>" . $row["Cantidad"] . "</td>
                <td>" . "$" . $row["Ganancia"] . "</td>
                <td>" . $row["Fecha"] . "</td>
                <td>" . $row["Hora"] . "</td>"; 
    }
    echo "</table><br>";
}else
    echo "No hay resultados";
?>

</div>

    <script src="../Scripts/buscarVenta.js"></script>
</body>
</html>