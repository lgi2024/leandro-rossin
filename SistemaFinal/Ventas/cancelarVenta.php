<?php
include '../Conexion/conexionBD.php';
session_start();

if (isset($_SESSION['carrito']) && count($_SESSION['carrito']) > 0) {
    foreach ($_SESSION['carrito'] as $item){
        $id = intval($item['id']);
        $cant = intval($item['cantidad']);

        $sql = "SELECT Stock FROM productos WHERE Id=$id";
        $result = mysqli_query($con, $sql);

        if (mysqli_num_rows($result) > 0){
            $fila = mysqli_fetch_assoc($result);
            $stockDisponible = $fila['Stock'];

            //Volvemos a sumar el stock cuando cancelamos la venta
            $stockDisponible = $stockDisponible + $cant;
            $sql = "UPDATE productos SET Stock='$stockDisponible' WHERE Id=$id";
                
            if(mysqli_query($con, $sql))
                $_SESSION['mensaje'] = 'cancelar';
        }
    }

    unset($_SESSION['carrito']); //Eliminamos lo que tiene la sesion de carrito
    
}

header("Location: ../Productos/listaProductos.php");
exit();
?>
