<?php
include '../Conexion/conexionBD.php';
session_start();

date_default_timezone_set('America/Argentina/Buenos_Aires');
$fecha = date('Y-m-d');
$hora = date('H:i:s');

if (isset($_SESSION['carrito']) && count($_SESSION['carrito']) > 0) {
    //Acá estamos extrayendo lo que contiene el carrito
    foreach ($_SESSION['carrito'] as $item){
        $id = intval($item['id']);
        $producto = mysqli_real_escape_string($con, $item['nombre']);
        $ganancia = intval($item['pCosto']);
        $cantidad = intval($item['cantidad']);
        $total = intval($item['total']);

        $sql = "INSERT INTO ventas(Id_producto, Producto, Cantidad, Ganancia, Fecha, Hora) VALUES
            ('$id', '$producto', '$cantidad', '$ganancia', '$fecha', '$hora')";
        mysqli_query($con, $sql);
    }

    unset($_SESSION['carrito']); //Eliminamos lo que tiene la sesion de carrito
    $_SESSION['mensaje'] = 'vendido';
}

header("Location: ../Productos/listaProductos.php");
exit();
?>
