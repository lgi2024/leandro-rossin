<?php
include '../Principal/pagPrincipal.php';
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';

$orden = isset($_POST['ordenarPor']) ? $_POST['ordenarPor'] : "idasc"; // Valor por defecto para ordenar por ID
$direccion = "ASC"; // Orden ascendente por defecto
?>


<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Productos</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListas.css">
    </head>
    <body>
        <div class="contenedor">
            <h1>Lista de Productos</h1>

            <!--Buscar datos-->
            <div class="busqueda">
                <label>Búsqueda:</label> <input type="text" id="busqueda" name="busqueda" 
                onkeyup="buscarProductos()" placeholder="Buscar...">
            </div>

            <!--Metodos de ordenamiento de la lista-->
            <form action="" method="POST">
                <label>Ordenar por:</label>
                <select name="ordenarPor" id="ordenarPor">
                    <option value="idasc" <?php if ($orden == 'idasc') echo 'selected'; ?>>Id (asc)</option>
                    <option value="iddesc" <?php if ($orden == 'iddesc') echo 'selected'; ?>>Id (desc)</option>
                    <option value="nombreasc" <?php if ($orden == 'nombreasc') echo 'selected'; ?>>Nombre (asc)</option>
                    <option value="nombredesc" <?php if ($orden == 'nombredesc') echo 'selected'; ?>>Nombre (desc)</option>
                    <option value="fechaasc" <?php if ($orden == 'fechaasc') echo 'selected'; ?>>Fecha (asc)</option>
                    <option value="fechadesc" <?php if ($orden == 'fechadesc') echo 'selected'; ?>>Fecha (desc)</option>
                </select>
                <input type="submit" value="Aplicar">
            </form>

            <!--Botones-->
            <?php if($_SESSION['Rol'] == 'Administrador'): ?>
                <form action="agregarProducto.php" id=agregar>
                    <button type="submit" id="btnAgregar">+ Nuevo Producto</button>
                </form>
            <?php endif; ?>
            <?php if ($_SESSION['Rol'] == 'Vendedor'): ?>
                <button id='agregarCarrito'>Vender</button>
                <button id='venta'>Ver carrito</button>
                <form action='../Ventas/listaVentas.php'>
                    <button id='verVentas'>Ver ventas</button>
                </form></br>
            <?php endif; ?>
        




<?php
//Hacemos la consulta
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $orden = $_POST['ordenarPor'];

    if ($orden == 'idasc' || $orden == 'iddesc')
        $campoOrden = 'Id';
    elseif ($orden == 'nombreasc' || $orden == 'nombredesc') 
        $campoOrden = 'Nombre';
    elseif ($orden == 'fechaasc' || $orden == 'fechadesc') 
        $campoOrden = 'Fecha';


    $direccion = ($orden == 'nombreasc' || $orden == 'idasc' || $orden == 'fechaasc') ? 'ASC' : 'DESC';
} else 
    $campoOrden = 'Id'; /*Cuando entramos a la ventana, al inicio no seleccionamos ninguna forma para ordenar
    la lista, entonces por defecto se ordenará por el id*/

$sql = "SELECT Foto, Id, Nombre, PrecioCosto, PrecioUnitario, Stock, Fecha, Hora FROM productos ORDER BY $campoOrden $direccion";
$result = mysqli_query($con, $sql);




//Mostramos los resultados de la consulta
if (mysqli_num_rows($result) > 0) {
    echo "<table id=tabla border='1'><tr> 
    <th>Foto</th>
    <th>Id</th> 
    <th>Nombre</th> 
    <th>Precio de Costo</th> 
    <th>Precio Unitario</th> 
    <th>Stock</th>
    <th>Fecha</th>
    <th>Hora</th>";
    if ($_SESSION['Rol'] == 'Administrador')
        echo "<th colspan='2'>Operaciones</th></tr>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td><img src='" . $row["Foto"] . "' alt='Foto' width='50'></td>
                <td>" . $row["Id"] . "</td>
                <td>" . $row["Nombre"] . "</td>
                <td>" . "$" . $row["PrecioCosto"] . "</td>
                <td>" . "$" . $row["PrecioUnitario"] . "</td>
                <td>" . $row["Stock"] . "</td>
                <td>" . $row["Fecha"] . "</td>
                <td>" . $row["Hora"] . "</td>"; 
                if ($_SESSION['Rol'] == 'Administrador') {
                    echo  
                    "<td><form action='modificarProducto.php' method='GET'>
                        <input type='hidden' name='mod' value='" . $row['Id'] . "'>
                        <button type='submit' id='btnModificar'>Modificar</button>
                    </form></td>";
                    echo 
                    "<td><form id='formEliminar" . $row['Id'] . "' action='eliminarProducto.php' method='POST'>
                        <input type='hidden' name='elim' value='" . $row['Id'] . "'>
                        <button type='button' id='btnEliminar' onclick='confirmarEliminacionProducto(" . $row['Id'] . ")'>Eliminar</button>
                    </form></td></tr>";
                }
    }
    echo "</table><br>";
} else 
    echo "No hay resultados";
?>
</div>


    <!--Ventana emergente boton Agregar al carrito-->
    <dialog id="carrito">
        <h2>VENTAS</h2>
        <form action="../Ventas/procesarVenta.php" method="POST">
            Seleccione un producto:
            <?php
                $sql = "SELECT Id, Nombre, Stock FROM productos";
                $result_dialogo = mysqli_query($con, $sql);
                if (mysqli_num_rows($result_dialogo) > 0) {
                    echo "<select name='mostrarProductos'>";
                    while ($row = mysqli_fetch_assoc($result_dialogo)) {
                        echo "<option value='" . $row['Id'] . "'>" . $row['Nombre'] . "</option>";
                    }
                    echo "</select>";
                }
            ?>
                <p></p>
            Cantidad: <input type="number" name="cant" required><br>
                <p></p>
            <button type="submit" id="agregar">Agregar al Carrito</button>
        </form>
                <p></p>
            <button id="cerrarVentana1">Cerrar</button>
    </dialog>
    
    
    <!--Ventana emergente boton Ver carrito-->
    <dialog id="verCarrito">
        <h2>Carrito de Ventas</h2>
        <h3>Productos agregados</h3>
        
        <?php if (isset($_SESSION['carrito']) && count($_SESSION['carrito']) > 0): ?>
            <?php
                $totalNeto = 0;
            ?>
            <table border="1">
                <tr>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Total</th>
                </tr>
                <!--Acá recorremos todo lo que tiene la sesion de carrito-->
                <?php foreach ($_SESSION['carrito'] as $item): ?>
                    <tr>
                        <td><?php echo $item['nombre']; ?></td>
                        <td><?php echo $item['cantidad']; ?></td>
                        <td><?php echo $item['precio']; ?></td>
                        <td><?php echo $item['total']; ?></td>
                    </tr>
                    <?php
                        $totalNeto += $item['total']; 
                    ?>
                <?php endforeach; ?>
                 
                <tr>
                <!--Mostramos el total de la venta-->
                <td colspan="3" style="text-align: right; font-weight: bold;">Total Neto:</td>
                <td><?php echo $totalNeto; ?></td>
                </tr>
            </table>
            <br>
            <form action="../Ventas/procesarVentaFinal.php" method="POST">
                <button id="confirmarVenta">Vender</button>
            </form>
            <br>
            <form action="../Ventas/cancelarVenta.php" method="POST">
                <button id="cancelar">Cancelar Venta</button>
            </form>
        <?php else: ?>
            <p>El carrito está vacío.</p>
        <?php endif; ?>
        
        <p></p>
        <button id="cerrarVentana2">Cerrar</button>
    </dialog>
    

    
    

    <!--JS-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../Scripts/buscarProductos.js"></script>
    <script src="../Scripts/ventas.js"></script>
    <script src="../Scripts/eliminar.js"></script>
    
    </body>
</html>


<?php
//Mensajes de confirmacion
if(isset($_SESSION['mensaje'])){
    if($_SESSION['mensaje'] == 'exito')
        echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: 'Producto agregado al carrito con éxito',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    elseif ($_SESSION['mensaje'] == 'error') 
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'No hay suficiente stock. Intentelo de nuevo',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    elseif ($_SESSION['mensaje'] == 'vendido')
        echo "<script>
        Swal.fire({
            icon: 'success',
            title: 'Éxito',
            text: 'Producto vendido exitosamente',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    elseif ($_SESSION['mensaje'] == 'invalido')
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Debe seleccionar una cantidad mayor a 0',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    elseif ($_SESSION['mensaje'] == 'cancelar') 
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Venta cancelada correctamente',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";

    unset($_SESSION['mensaje']);
}
?>