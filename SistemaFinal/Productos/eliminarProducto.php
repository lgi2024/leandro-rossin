<html>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>
</html>

<?php
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';
//session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST['elim'])){
        $id = $_POST['elim'];

        $sql = "DELETE FROM productos WHERE Id=$id";
        if (mysqli_query($con, $sql)) 
            echo "<script>
                    Swal.fire({
                        title: 'Producto eliminado correctamente',
                        icon: 'success'
                    }).then(() => {
                        window.location.href = 'listaProductos.php';
                    });
                </script>";
        else
            echo "<script>
                Swal.fire({
                    title: 'Error al eliminar el producto',
                    icon: 'error'
                });
            </script>";
    }
}
