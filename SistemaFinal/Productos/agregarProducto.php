<?php
include '../Conexion/conexionBD.php';
include '../Principal/manejoImagen.php';
include '../Inicio/validarSesion.php';
?>



<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Productos</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estAgregar.css">
    </head>
    <body>
    <div class="container">
        <h1>ESENCAP</h1>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    
        
        <!--Crear nuevo producto-->
        <form action="" method="POST" enctype="multipart/form-data">
            <fieldset>
                <legend>Añadir Nuevo Producto</legend>
                Nombre: <input type="text" name="nombre" required><br>
                Precio Unitario: <input type="number" name="precio" required><br>
                Stock: <input type="number" name="stock" required><br>
                Seleccionar Imagen del Producto: <input type="file" name="foto" id="foto"><br><br>

                Insumos Necesarios
                <div class="insumos-container">
                    <select id="insumo" name="insumo">
                        <?php
                        $consulta = "SELECT Id, Nombre, Stock FROM insumos";
                        $result = mysqli_query($con, $consulta);
                        while ($row = mysqli_fetch_assoc($result)) {
                            echo "<option value='".$row['Id']."'>".$row['Nombre']." (Stock: ".$row['Stock'].")</option>";
                        }
                        ?>
                    </select>
                    <input type="number" name="cantidad" id="cantidad" placeholder="Cantidad" min="1" required>
                </div>
                <button type="button" onclick="agregarInsumo()">Agregar Insumo</button>
                <button type="button" onclick="borrarInsumo()">Borrar Insumo</button>
            
            <div class="cancelar">
                <input type="submit" value="Agregar Producto">
                <input class="volver" type="button" value="Cancelar" onclick="window.history.back();">
            </div>
            </fieldset>
        </form>



        <!--<form action="insumo_producto.php" method="POST" enctype="multipart/form-data">
            Insumos Necesarios:
                <?php
                    /*$consulta = "SELECT Id, Nombre FROM insumos";
                    $result = mysqli_query($con, $consulta);
                    if (mysqli_num_rows($result) > 0) {
                        echo "<select id='insumo' name='insumo'>";
                        while ($row = mysqli_fetch_assoc($result)) 
                            echo "<option value='" . $row['Id'] . "'>" . $row['Nombre'] . "</option>";
                        echo "</select><br>";
                    }*/
                ?>
                <input type="number" id="cantidad" name="cantidad" placeholder="Cantidad" min="1" required>
                <input type="submit" value="Agregar Insumo">
        </form>-->
            

        



<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['nombre'];
    $precio = $_POST['precio'];
    $stock = $_POST['stock'];
    $precioCosto = 0;
    $target_file = "NULL";

    if (isset($_SESSION['insumosDelProducto']) && count($_SESSION['insumosDelProducto']) > 0){
        foreach ($_SESSION['insumosDelProducto'] as $item){
            $precioCosto=$precioCosto + ($item['cantIns']*$item['precioU']);
        }
    }

    

    // Verificar si se ha subido una nueva foto
    if (!empty($_FILES["foto"]["name"])) {
        $uploadResult = uploadImage($_FILES["foto"]);
        if ($uploadResult['status'] == 1) {
            $target_file = $uploadResult['path'];
        } else {
            echo $uploadResult['message'];
            exit();
        }
    }

    // Insertar en la base de datos
    if ($precio >= 0) {
        if ($stock > 0){
            $sql = "INSERT INTO productos (Foto, Nombre, PrecioCosto, PrecioUnitario, Stock) 
            VALUES ('$target_file', '$nombre', '$precioCosto', '$precio', '$stock')";
            
            if (mysqli_query($con, $sql)){
                $productoId = mysqli_insert_id($con);

                foreach ($_SESSION['insumosDelProducto'] as $item){
                    $idInsumo = $item['idIns'];
                    $cantInsumo = $item['cantIns'];

                    $sql2 = "INSERT INTO productos_insumos (Id_producto, Id_insumo, Cantidad) 
                    VALUES ($productoId, $idInsumo, $cantInsumo)";
                    mysqli_query($con, $sql2);
                }
                
                echo "<script>
                    Swal.fire({
                        icon: 'success',
                        title: 'Éxito',
                        text: 'Nuevo producto agregado con exito',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    </script>";
                    //Esto evita que el mensaje de confirmación aparezca cada vez que se recarge la página
                    echo "<script>setTimeout(function(){ window.location.href = 'agregarProducto.php'; }, 2000);</script>";
                    exit();
            }
            else 
                echo "<script>
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un problema al agregar el producto. Inténtelo de nuevo.',
                        showConfirmButton: false,
                        timer: 3000
                    });
                    </script>";
        } else 
            echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El stock debe ser mayor o igual a 0. Intentelo de nuevo',
                showConfirmButton: false,
                timer: 3000
            });
            </script>";
    } else 
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'El precio debe ser mayor o igual a 0. Intentelo de nuevo',
            showConfirmButton: false,
            timer: 3000
        });
        </script>";
    
    
}
unset($_SESSION['insumosDelProducto']);
?>

</div>

<script src="../Scripts/insumo_producto.js"></script>
</body>
</html>