<?php
include '../Conexion/conexionBD.php';
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $insumo_id = intval($_POST['insumo_id']);
    $cantidad = intval($_POST['cantidad']);

    // Obtener el stock actual del insumo
    $queryStock = "SELECT Stock, PrecioUnitario FROM insumos WHERE Id='$insumo_id'";
    $result = mysqli_query($con, $queryStock);
    if ($row = mysqli_fetch_assoc($result)) {
        $stockActual = $row['Stock'];
        $precioUnitario = $row['PrecioUnitario'];

        if ($cantidad <= $stockActual) {
            // Restar el stock
            $nuevoStock = $stockActual - $cantidad;
            $queryActualizar = "UPDATE insumos SET Stock='$nuevoStock' WHERE Id='$insumo_id'";
            
            if (mysqli_query($con, $queryActualizar)) {
                $insumosSeleccionados=[
                    'idIns' => $insumo_id,
                    'cantIns' => $cantidad,
                    'precioU' => $precioUnitario
                ];

                if (isset($_SESSION['insumosDelProducto'])) 
                    $_SESSION['insumosDelProducto'][] = $insumosSeleccionados;
                else 
                    $_SESSION['insumosDelProducto'] = [$insumosSeleccionados];

            } else 
                echo "Error al actualizar el stock.";
        } else 
            echo "Error: El stock es insuficiente.";
    } else 
        echo "Error: Insumo no encontrado.";
}
?>