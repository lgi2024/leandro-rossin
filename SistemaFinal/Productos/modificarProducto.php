<?php
include '../Inicio/validarSesion.php';
include '../Conexion/conexionBD.php';
include '../Principal/manejoImagen.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $nombre = $_POST['nombre'];
    $precio = $_POST['precio'];
    $stock = $_POST['stock'];
    $actualizarFoto = false;
    $target_file = "";

    // Verificar si se ha subido una nueva foto
    if (!empty($_FILES["foto"]["name"])) {
        $uploadResult = uploadImage($_FILES["foto"]);
        if ($uploadResult['status'] == 1) {
            $target_file = $uploadResult['path'];
            $actualizarFoto = true;
        } else {
            echo $uploadResult['message'];
            exit();
        }
    }

    if ($precio > 0) {
        if ($stock > 0) {
            //Consulta
            if ($actualizarFoto) {
                $sql = "UPDATE productos SET Foto='$target_file', Nombre='$nombre', PrecioUnitario='$precio', Stock='$stock' WHERE Id='$id'";
            } else {
                $sql = "UPDATE productos SET Nombre='$nombre', PrecioUnitario='$precio', Stock='$stock' WHERE Id='$id'";
            }

            // Ejecutar consulta
            if (mysqli_query($con, $sql)) {
                echo "<br>Datos actualizados con éxito";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($con);
            }
        } else {
            echo "El stock disponible debe ser mayor a 0.";
        }
    } else {
        echo "El precio debe ser mayor a 0.";
    }
}

/////////////////////////////////////////////////////////////////////

//Recibimos el id
$id = $_GET['mod'];

// Obtener los datos del producto
$sql = "SELECT Foto, Id, Nombre, PrecioUnitario, Stock FROM productos WHERE Id=$id";
$result = mysqli_query($con, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $nombre = $row['Nombre'];
    $precio = $row['PrecioUnitario'];
    $stock = $row['Stock'];
} else {
    echo "No se encontró el producto con ID: $id";
    exit();
}
mysqli_close($con);
?>

<form method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    Nombre del Producto: <input type="text" name="nombre" value="<?php echo $nombre; ?>" required><br>
    Precio Unitario: <input type="number" name="precio" value="<?php echo $precio; ?>" required><br>
    Stock disponible: <input type="number" name="stock" value="<?php echo $stock; ?>" required><br>
    Seleccionar una nueva imagen para cargar (opcional): <input type="file" name="foto" id="foto"><br>
    <input type="submit" value="Actualizar">
    <a href='listaProductos.php'> Volver atrás </a>
</form>