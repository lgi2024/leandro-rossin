function buscarVenta() {
    let input = document.getElementById("busqueda").value.toLowerCase();
    let filas = document.getElementsByTagName("tr");

    for (let i = 1; i < filas.length; i++) { // Comenzar en 1 para omitir los encabezados
        let codProducto = filas[i].getElementsByTagName("td")[1].innerText.toLowerCase();
        let producto = filas[i].getElementsByTagName("td")[2].innerText.toLowerCase();
        let fecha = filas[i].getElementsByTagName("td")[5].innerText.toLowerCase();

        if (codProducto.includes(input) || producto.includes(input) || fecha.includes(input)) {
            filas[i].style.display = "";  // Mostrar fila
        } else {
            filas[i].style.display = "none";  // Ocultar fila
        }
    }
}