function buscarProductos() {
    let input = document.getElementById("busqueda").value.toLowerCase();
    let filas = document.getElementsByTagName("tr");

    for (let i = 1; i < filas.length; i++) { // Comenzar en 1 para omitir los encabezados
        let id = filas[i].getElementsByTagName("td")[1].innerText.toLowerCase();
        let nombre = filas[i].getElementsByTagName("td")[2].innerText.toLowerCase();

        if (id.includes(input) || nombre.includes(input)) {
            filas[i].style.display = "";  // Mostrar fila
        } else {
            filas[i].style.display = "none";  // Ocultar fila
        }
    }
}