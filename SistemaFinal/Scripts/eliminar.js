src="https://cdn.jsdelivr.net/npm/sweetalert2@11"

    function confirmarEliminacionProducto(id) {
        Swal.fire({
            title: '¿Estás seguro de eliminar este producto?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('formEliminar' + id).submit();
            }
        });
    }

    function confirmarEliminacionInsumo(id) {
      Swal.fire({
          title: '¿Estás seguro de eliminar este insumo?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Confirmar'
      }).then((result) => {
          if (result.isConfirmed) {
              document.getElementById('formEliminarIn' + id).submit();
          }
      });
    }

    function confirmarEliminacionProv(id) {
        Swal.fire({
            title: '¿Estás seguro de eliminar a este proveedor?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar'
        }).then((result) => {
            if (result.isConfirmed) {
                document.getElementById('formEliminarp' + id).submit();
            }
        });
    }
