src="https://cdn.jsdelivr.net/npm/sweetalert2@11"
let insumosSeleccionados = [];

function agregarInsumo() {
    let nuevoInsumo = document.getElementById("insumo");
    let nuevaCantidad = document.getElementById("cantidad");

    let insumoId = nuevoInsumo.value;
    let cantidad = parseInt(nuevaCantidad.value);

    if (!insumoId || isNaN(cantidad) || cantidad <= 0) {
        alert("Debe seleccionar un insumo y una cantidad válida.");
        return;
    }

    

    // Enviar la actualización del stock al servidor
    fetch("../Productos/actualizarStock.php", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: `insumo_id=${insumoId}&cantidad=${cantidad}`
    })
    .then(response => response.text())
    .then(data => {
        console.log(data);
        if (data.startsWith("Error")) {
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'El stock es insuficiente',
                showConfirmButton: false,
                timer: 3000
            });
        } else {
            alert("Insumo agregado correctamente");
        }
    })
    .catch(error => console.error("Error en la actualización:", error));
}


function borrarInsumo(){
    
}