//Dialog Agregar al carrito
const btnAgregarCarrito = document.querySelector("#agregarCarrito");
const btnCerrar1 = document.querySelector("#cerrarVentana1");
const ventana1 = document.querySelector("#carrito");

//Dialog Ver carrito
const btnCarrito = document.querySelector("#venta");
const btnCerrar2 = document.querySelector("#cerrarVentana2");
const ventana2 = document.querySelector("#verCarrito");



btnAgregarCarrito.addEventListener("click",()=>{
    ventana1.showModal();
})
btnCerrar1.addEventListener("click",()=>{
    ventana1.close();
})

btnCarrito.addEventListener("click", ()=>{
    ventana2.showModal();
})
btnCerrar2.addEventListener("click",()=>{
    ventana2.close();
})
