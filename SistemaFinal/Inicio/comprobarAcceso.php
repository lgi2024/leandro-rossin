<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Comprobar Acceso</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListaProd.css">
    </head>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>


<?php
    include '../Conexion/conexionBD.php';

    //Control de inicio de sesion
    session_start(); 

    extract($_REQUEST);
    $sql = "SELECT Rol FROM usuarios WHERE Usuario='$usuario' AND Contrasena='$contrasena'";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) > 0){
        while ($row = mysqli_fetch_assoc($result)) {
            $_SESSION['Rol'] = $row["Rol"];
            if($row["Rol"]=="Administrador"){
                header('Location: ../Principal/pagInicio.php');
                exit;
            }
            else{
                header('Location: ../Productos/listaProductos.php');
                exit;
            }
        }
    }
    else{
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Usuario no encontrado, intentelo de nuevo',
            showConfirmButton: false,
            timer: 3000
        }).then(() => {
            window.location.href = 'iniciarSesion.html';
        });
        </script>";
    }
?>   
