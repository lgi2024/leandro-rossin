<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Agregar Usuario</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estListaProd.css">
    </head>
    <body>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    </body>

    <?php
    include '../Conexion/conexionBD.php';
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nombre = $_POST['usuario'];
        $cont = $_POST['contrasena'];
        $correo = $_POST['correo'];
        $rol = $_POST['rol'];

        $sql = "INSERT INTO usuarios (Usuario, Correo, Contrasena, Rol) VALUES ('$nombre', '$correo', '$cont', '$rol')";

        if(mysqli_query($con, $sql))
            echo "<script>
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Usuario agregado correctamente',
                showConfirmButton: false,
                timer: 3000
            });
            </script>";
        else
            echo "<script>
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'No se pudo agregar el nuevo usuario',
                showConfirmButton: false,
                timer: 3000
            });
            </script>";

        if($rol=="Administrador"){
            header('Location: ../Principal/pagPrincipal.php');
            exit;
        }
        elseif($rol=="Vendedor"){
            header('Location: ../Productos/listaProductos.php');
            exit;
        }
    }
    
    ?>