<?php
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Nuevo Usuario</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estIS.css">
    </head>
    <body>
        <div class="login-container">
        <h1>ESENCAP</h1>
        <form action="agregarUsuario.php" method="POST" onsubmit="return validacion()">
            <p>Ingrese un nombre de usuario: <br/>
                <input id="usuario" name="usuario" type="text" size="50"/>
            </p>
            <p>Ingrese una contraseña: <br/>
                <input id="contrasena" name="contrasena" type="password" size="50"/>
            </p>
            <p>Ingrese un correo electrónico: <br/>
                <input id="correo" name="correo" type="email" size="50"/>
            </p>
            <p>Rol de usuario: <br/>
                <?php
                $sql = "SELECT DISTINCT Rol FROM usuarios";
                $resultado = mysqli_query($con, $sql);
                    if (mysqli_num_rows($resultado) > 0) {
                        echo "<select name='rol'>";
                        while ($row = mysqli_fetch_assoc($resultado)) {
                            echo "<option value='" . $row['Rol'] . "'>" . $row['Rol'] . "</option>";
                        }
                        echo "</select><br>";
                    }
                ?>
            </p>
                <input id="enviar" value="Crear" type="submit" size="50"/>
        </form>
    </div>
        
        <!--Script para validar que ingresemos algo-->
        <script src="../Scripts/validacion.js"></script>
    </body>
</html>