<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../Estilos/estPrincipal.css">
</head>
<body>
    <header>
        <div class="logo">
            <h2 class="logo-empresa">ESENCAP</h2>
        </div>
        <nav>
            <?php if($_SESSION['Rol'] == 'Administrador'): ?>
                <a href="../Principal/pagInicio.php" class="nav-link">Inicio</a>
                <a id="contacto" href="#" class="nav-link">Contacto</a>
                <a href="../Inicio/nuevoUsuario.php" class="nav-link">Nuevo Usuario</a>
                <a href="../Inicio/cerrarSesion.php" class="nav-link">Cerrar Sesión</a>
            <?php elseif($_SESSION['Rol'] == 'Vendedor'): ?>
                <a id="contacto" href="#" class="nav-link">Contacto</a>
                <a href="../Inicio/cerrarSesion.php" class="nav-link">Cerrar Sesión</a>
            <?php endif; ?>
        </nav>
    </header>

    <dialog id="dialogContacto">
        <h2>Información de Contacto</h2>
        <label>WhatsApp: 3743-590966</label><br>
        <label>Email: leafabian26@gmail.com</label><br><br>
        <button id="cerrarDialog">Cerrar</button>
    </dialog>
    
    <script src="../Scripts/dialogContacto.js"></script>
</body>
</html>