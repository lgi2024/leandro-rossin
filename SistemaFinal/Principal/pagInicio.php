<?php
include 'pagPrincipal.php';
include '../Inicio/validarSesion.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
    <link rel="stylesheet" type="text/css" href="../Estilos/estInicio.css">
</head>
<body>
    <section>
        <a href="../Productos/listaProductos.php">
            <img src="../CargasImg/prod.jpg" alt="">
            <span>PRODUCTOS</span>
        </a>
        <a href="../Insumos/listaInsumos.php">
            <img src="../CargasImg/insumos.jpg" alt="">
            <span>INSUMOS</span>
        </a>
        <a href="../Proveedores/listaProveedores.php">
            <img src="../CargasImg/proveed.jpg" alt="">
            <span>PROVEEDORES</span>
        </a>
        <a href="../Ventas/listaVentas.php">
            <img src="../CargasImg/ventas1.jpg" alt="">
            <span>VENTAS</span>
        </a>
    </section>
    
</body>
</html>