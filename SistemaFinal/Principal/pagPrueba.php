<?php
include '../Conexion/conexionBD.php';
include '../Inicio/validarSesion.php';
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Página Principal</title>
        <link rel="stylesheet" type="text/css" href="../Estilos/estilos.css">
    </head>
    <body>
        <div class="contenedor">
            <h1>ESENCAP</h1>
            <nav>
                <ul class="menu">
                    <li><a href="pagPrincipal.php">Inicio</a></li>
                    <li><a href="../Productos/listaProductos.php">Productos</a></li>
                    <li><a href="../Insumos/listaInsumos.php">Insumos</a></li>
                    <li><a href="../Proveedores/listaProveedores.php">Proveedores</a></li>
                    <li><a href="../Ventas/listaVentas.php">Ventas</a></li>
                    <li><a href="../Inicio/nuevoUsuario.php">Nuevo Usuario</a></li>
                    <li><a href="../Inicio/cerrarSesion.php">Cerrar Sesión</a></li>
                </ul>
            </nav>
        </div>
        

        <section id="content">
            <form action="../Productos/agregarProducto.php">
                <input type="submit" value="+ Agregar Productos">
            </form>

            <form action="../Insumos/agregarInsumo.php">
                <input type="submit" value="+ Agregar Insumos">
            </form>

            <form action="../Proveedores/agregarProv.php">
                <input type="submit" value="+ Agregar Proveedores">
            </form>
        </section>
    </body>
</html>