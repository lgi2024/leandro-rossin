<?php
include '../Conexion/conexionBD.php';

// Obtener las fechas únicas (etiquetas del eje X)
$consultaFechas = "SELECT DISTINCT Fecha FROM ventas ORDER BY Fecha";
$resultFechas = mysqli_query($con, $consultaFechas);
$fechas = [];
while ($row = mysqli_fetch_assoc($resultFechas)) {
    $fechas[] = $row["Fecha"];
}

// Obtener los productos y sus ventas por fecha
$consultaProductos = "SELECT Producto, Fecha, SUM(Cantidad) as total FROM ventas GROUP BY Producto, Fecha ORDER BY Producto, Fecha";
$resultProductos = mysqli_query($con, $consultaProductos);

$datos = [];
while ($row = mysqli_fetch_assoc($resultProductos)) {
    $producto = $row["Producto"];
    $fecha = $row["Fecha"];
    $cantidad = $row["total"];

    if (!isset($datos[$producto])) {
        $datos[$producto] = array_fill_keys($fechas, 0); // Inicializar con ceros
    }

    $datos[$producto][$fecha] = $cantidad; // Asignar ventas a la fecha correspondiente
}

// Convertir datos a JSON para JavaScript
$fechasJSON = json_encode($fechas);
$datosJSON1 = json_encode($datos);
?>