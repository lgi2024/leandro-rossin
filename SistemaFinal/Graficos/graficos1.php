<?php
include 'graf1logica.php';
include 'graf3logica.php';
include '../Inicio/validarSesion.php';
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gráfico</title>
    <link rel="stylesheet" type="text/css" href="../Estilos/estilos.css">
</head>
<body>
    <div style="width: 700px; height: 700px;">
        <canvas id="ventasProductos"></canvas>
    </div>

    <div style="width: 700px; height: 700px;">
        <canvas id="ventasTotales"></canvas>
    </div>

    <div style="width: 700px; height: 700px;">
        <canvas id="gananciaPorProducto"></canvas>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        //Traemos los datos
        const fechas = <?php echo $fechasJSON; ?>;
        const datos1 = <?php echo $datosJSON1; ?>;

        //Colores para dataset
        const colores = ['red', 'blue', 'green', 'orange', 'purple', 'cyan', 'magenta'];

        //Crear datasets dinámicamente
        const datasets1 = Object.keys(datos1).map((producto, index) => ({
            label: producto,
            data: fechas.map(fecha => datos1[producto][fecha]), //Asignar ventas por fecha
            borderColor: colores[index % colores.length],
            borderWidth: 2,
            fill: false
        }));


        //Configuración gráfico 1
        const ctx = document.getElementById('ventasProductos').getContext("2d");
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: fechas,
                datasets: datasets1
            },
            options: {
                plugins: { 
                    title: {
                        display: true,
                        text: 'Cantidad de ventas de los productos por día',
                        font: {  
                            size: 20
                        },
                        padding: 20 
                    },
                    legend: {
                        position: 'bottom',
                        labels: {
                            font: {
                                size: 14 
                            },
                            padding: 20, 
                            boxWidth: 15,
                            fontFamily: 'system-ui',
                            fontColor: 'black',
                            color: '#333' 
                        }
                    }
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });




        //Configuración gráfico 2
        const ctxd = document.getElementById('ventasTotales').getContext("2d");
        new Chart(ctxd, {
            type: 'bar',
            data: {
                labels: fechas,
                datasets: [
                    {
                        label: 'Total',
                        data: [
                            <?php
                                $sql = "SELECT Fecha, SUM(Cantidad) as total FROM ventas GROUP BY Fecha";
                                $result = mysqli_query($con, $sql);
                                while ($registro = mysqli_fetch_assoc($result)){?>
                                    '<?php echo $registro["total"] ?>',
                                <?php
                                } 
                            ?>
                        ],
                        borderWidth: 2
                    }
                ],
            },
            options: {
                plugins: { 
                    title: {
                        display: true,
                        text: 'Total de ventas por día',
                        font: {  
                            size: 20
                        },
                        padding: 20 
                    },
                    legend: {
                        position: 'bottom',
                        labels: {
                            font: {
                                size: 14 
                            },
                            padding: 20, 
                            boxWidth: 15,
                            fontFamily: 'system-ui',
                            fontColor: 'black',
                            color: '#333' 
                        }
                    }
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });




        const fechas2 = <?php echo $fechasJSON2; ?>;
        const datos2 = <?php echo $datosJSON2; ?>;

        //Colores para dataset
        const colores2 = ['red', 'blue', 'green', 'orange', 'purple', 'cyan', 'magenta'];

        //Crear datasets dinámicamente
        const datasets2 = Object.keys(datos2).map((producto, index) => ({
            label: producto,
            data: fechas.map(fecha => datos2[producto][fecha]), //Asignar ventas por fecha
            borderColor: colores2[index % colores.length],
            borderWidth: 2,
            fill: false
        }));

        //Configuración gráfico 3
        const ctx3 = document.getElementById('gananciaPorProducto').getContext("2d");
        new Chart(ctx3, {
            type: 'line',
            data: {
                labels: fechas2,
                datasets: datasets2
            },
            options: {
                plugins: { 
                    title: {
                        display: true,
                        text: 'Ganancias por Producto',
                        font: {  
                            size: 20
                        },
                        padding: 20 
                    },
                    legend: {
                        position: 'bottom',
                        labels: {
                            font: {
                                size: 14 
                            },
                            padding: 20, 
                            boxWidth: 15,
                            fontFamily: 'system-ui',
                            fontColor: 'black',
                            color: '#333' 
                        }
                    }
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    </script>
</body>
</html>
