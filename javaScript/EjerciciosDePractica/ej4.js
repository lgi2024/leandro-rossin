const usuario = { id: 1 };

usuario.name = 'Leandro';
usuario.guardar = function(){
    console.log("Nombre guardado", usuario.name);
}

usuario.guardar();
console.log(usuario);

const persona = Object.seal({ id: 1 });

persona.name = "Lea";
console.log(persona);