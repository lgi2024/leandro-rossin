let arreglo = [5, 7, 100, 3, 1, 6, 9, -5];
let positivo=0;

function cuantosPositivos(arreglo){
    for (pos of arreglo){
        positivo += pos > 0 ? 1 : 0; 
    }
    return positivo;
}

let resultado = cuantosPositivos(arreglo);
console.log("Cantidad de nros positivos: " + resultado);