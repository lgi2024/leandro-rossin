<?php
// Verificar si los datos fueron enviados
if (isset($_POST['nombre']) && isset($_POST['edad'])) {
    $nombre = htmlspecialchars($_POST['nombre']); //Chequeo de contenido
    $edad = intval($_POST['edad']);

    // Validar que los datos no estén vacíos
    if (!empty($nombre) && $edad > 0) {
        echo "Hola, $nombre. Tienes $edad años.";
    } else {
        echo "Error: Por favor ingresa datos válidos.";
    }
} else {
    echo "Error: No se recibieron los datos.";
}
?>