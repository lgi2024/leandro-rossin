<html>
    <head>
        <title>Pedidos</title>
    </head>
    <body>
<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "pedidos";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    $sql = "SELECT id, nombre_producto, precio FROM productos";
    $result = mysqli_query($conn, $sql);
?>

        <form action="toma_de_pedidosconbd.php" method="get">
            <p>Ingrese nombre: <br/>
                <input type="text" size="50" name="nombre" />
                </p>
            <p>Ingrese contrasena: <br/>
                <input type="password" size="50" name="contrasena" />
                </p>
            <p>Pedido <br/>

            <?php
            if (mysqli_num_rows($result) > 0) {
            // output data of each row
                while($row = mysqli_fetch_assoc($result)) {
                    echo '<input type="checkbox" name="productos[]" value="' . $row["id"] . '"> ';
                    echo "producto: " . $row["nombre_producto"]. " - Precio: " . $row["precio"] . "<br>";
                }
            } else {
                echo "0 results";
            }

mysqli_close($conn);
?>
                </p>
            
                <label for="entrega">Tipo de entrega:</label>
                <select name="entrega" id="entrega">
                  <option value="1">Con delivery</option>
                  <option value="2">Paso a retirar</option>
                </select> 
                <br/>
                <br/>
            <input type="submit" size="50" name="enviar"/>
        </form>
    </body>
</html>