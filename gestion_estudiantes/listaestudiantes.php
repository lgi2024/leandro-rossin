<?php
include 'CreacionBD/conexion.php';


$orden = "id"; // Valor por defecto para ordenar por ID
$direccion = "ASC"; // Orden ascendente por defecto

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $orden = $_POST['ordenarPor'];

    if ($orden == 'nombreasc' || $orden == 'nombredesc') {
        $campoOrden = 'nombre';
    } elseif ($orden == 'edadasc' || $orden == 'edaddesc') {
        $campoOrden = 'edad';
    } elseif ($orden == 'emailasc' || $orden == 'emaildesc') {
        $campoOrden = 'email';
    }

    $direccion = ($orden == 'nombreasc' || $orden == 'edadasc' || $orden == 'emailasc') ? 'ASC' : 'DESC';
} else {
    $campoOrden = 'id'; //Si recien entramos a la ventana, veremos la lista ordenada por el id
}

$sql = "SELECT foto, id, nombre, edad, email FROM estudiantes ORDER BY $campoOrden $direccion";
$result = mysqli_query($conn, $sql);
?>




<!DOCTYPE html>
<html>
<head>
    <title>Lista de Estudiantes</title>
    <link rel="stylesheet" type="text/css" href="Estilos/estLista.css">
</head>
<body>
    <!--Busqueda-->
    <form method="get" action="busquedadedatos.php">
        Realizar una búsqueda: <input type="text" id="busqueda" name="busqueda" 
        onkeyup="buscarEstudiantes()" placeholder="Buscar...">
        <input type="submit" value="Buscar">
    </form>

    <form action="" method="post">
        <label for="ordenarPor">Ordenar por:</label>
        <select name="ordenarPor" id="ordenarPor">
            <option value="nombreasc" <?php if ($orden == 'nombreasc') echo 'selected'; ?>>Nombre (ascendente)</option>
            <option value="nombredesc" <?php if ($orden == 'nombredesc') echo 'selected'; ?>>Nombre (descendente)</option>
            <option value="edadasc" <?php if ($orden == 'edadasc') echo 'selected'; ?>>Edad (ascendente)</option>
            <option value="edaddesc" <?php if ($orden == 'edaddesc') echo 'selected'; ?>>Edad (descendente)</option>
            <option value="emailasc" <?php if ($orden == 'emailasc') echo 'selected'; ?>>Email (ascendente)</option>
            <option value="emaildesc" <?php if ($orden == 'emaildesc') echo 'selected'; ?>>Email (descendente)</option>
        </select>
        <input type="submit" value="Ordenar">
    </form>
        <!--Popup para mostrar los estudiantes con el cursor en cada fila-->
        <div id="popup" style="display:none; position:absolute; background-color:lightgray; padding:5px; border:1px solid black;"></div>
        
    </body>
</html>


<?php
session_start();

if (!isset($_SESSION['rol'])) {
    // Redireccionar o mostrar un mensaje de error si el rol no está definido
    echo "No tienes acceso a esta página.";
    exit;
}

//Muestra de resultados de la consulta
if (mysqli_num_rows($result) > 0) {
    echo "<h3>Lista de estudiantes</h3>";
    echo "<table id=tabla border='1'><tr> 
    <th>Foto</th> 
    <th>ID</th> 
    <th>Nombre</th> 
    <th>Edad</th> 
    <th>Correo Electrónico</th>";
    if ($_SESSION['rol'] == 'admin')
        echo "<th>Operaciones</th></tr>";
    while ($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td><img src='" . $row["foto"] . "' alt='Foto' width='50'></td>
                <td>" . $row["id"] . "</td>
                <td>" . $row["nombre"] . "</td>
                <td>" . $row["edad"] . "</td>
                <td>" . $row["email"] . "</td>"; 
                if ($_SESSION['rol'] == 'admin') {
                    echo "<td><a href='modificarphp_gestionestudiantes.php?id=" . $row["id"] . "'>Modificar</a></td>
                    <td><a href='eliminarphp_gestionestudiantes.php?id=" . $row["id"] . "'>Eliminar</a></td></tr>";
                }
    }
    echo "</table><br>";
} else {
    echo "0 resultados";
}
mysqli_close($conn);
?>

<!--Volver atrás-->
<form action="paginaprincipal.php">
    <input type="submit" value="Volver a la Pagina Principal">
</form>

<body>
    <!--Scripts-->
    <script src="Scripts/filtrarBusqueda.js"></script>
    <script src="Scripts/infoEstud.js"></script>
    <script src="Scripts/resaltarFilas.js"></script>
</body>