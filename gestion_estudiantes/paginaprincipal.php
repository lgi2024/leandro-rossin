<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Pagina Principal</title>
        <link rel="stylesheet" type="text/css" href="Estilos/estPagPrincipal.css">
    </head>
    <body>
        <h1>Gestión de Estudiantes</h1>



<!--Crear nuevo estudiante-->
<form method="post" action="" enctype="multipart/form-data">
    <fieldset>
        <legend>Información Personal</legend>
        Nombre: <input type="text" name="nombre" required><br>
        Edad: <input type="number" name="edad" required><br>
        Correo electrónico: <input type="email" name="email" required><br>
        Seleccionar una imagen para cargar: <input type="file" name="foto" id="foto" required><br>
    </fieldset>
        <input type="submit" value="Crear nuevo estudiante">
</form>


<?php
include 'CreacionBD/conexion.php';
include 'imagenes.php';

//Realizamos la consulta para crear un nuevo estudiante
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['nombre'];
    $age = $_POST['edad'];
    $email = $_POST['email'];
    $uploadResult = uploadImage($_FILES["foto"]);

    if ($uploadResult['status'] == 1) {
        $target_file = $uploadResult['path'];

        // Insertar en la base de datos
        if ($age >= 0) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $sql = "INSERT INTO estudiantes (foto, nombre, edad, email) VALUES ('$target_file', '$name', $age, '$email')";
                
                if (mysqli_query($conn, $sql)) {
                    echo "<br>Nuevo estudiante creado con éxito";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            } else {
                echo "El correo electrónico no tiene un formato válido.";
            }
        } else {
            echo "La edad debe ser mayor a 0.";
        }
    }else {
        echo $uploadResult['message'];
    }
}
?>


<!--Ver lista de estudiantes-->
<form action="listaestudiantes.php">
    <br><input type="submit" value="Ver Lista de Estudiantes">
</form>

<!--Cerrar Sesion-->
<form action="cerrarsesion.php">
    <br><input type="submit" value="Cerrar Sesion">
</form>

</body>
</html>

