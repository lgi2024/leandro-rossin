document.addEventListener("DOMContentLoaded", function() {
    const popup = document.getElementById('popup');
    
    // Seleccionar todas las filas de la tabla (excepto la primera, que es el encabezado)
    const filas = document.querySelectorAll('#tabla tr');

    filas.forEach(function(fila) {
        fila.addEventListener('mouseover', function(event) {
            const nombre = fila.cells[2].innerText; // Toma la tercera celda (nombre)
            const edad = fila.cells[3].innerText;   // Toma la cuarta celda (edad)

            // Mostrar el contenido del popup
            popup.innerHTML = `Nombre: ${nombre} <br> Edad: ${edad}`;
            popup.style.display = 'block'; // Mostrar el popup
        });

        fila.addEventListener('mousemove', function(event) {
            // Hacer que el popup siga al mouse
            popup.style.left = event.pageX + 10 + 'px';
            popup.style.top = event.pageY + 10 + 'px';
        });

        fila.addEventListener('mouseout', function() {
            // Ocultar el popup cuando el mouse salga de la fila
            popup.style.display = 'none';
        });
    });
});
