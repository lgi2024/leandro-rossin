function buscarEstudiantes() {
    let input = document.getElementById("busqueda").value.toLowerCase();
    let filas = document.getElementsByTagName("tr");

    for (let i = 1; i < filas.length; i++) { // Comenzar en 1 para omitir los encabezados
        let nombre = filas[i].getElementsByTagName("td")[1].innerText.toLowerCase();
        let apellido = filas[i].getElementsByTagName("td")[2].innerText.toLowerCase();

        if (nombre.includes(input) || apellido.includes(input)) {
            filas[i].style.display = "";  // Mostrar fila
        } else {
            filas[i].style.display = "none";  // Ocultar fila
        }
    }
}