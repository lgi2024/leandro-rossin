// Selecciona todas las filas de la tabla excepto la de encabezado
document.querySelectorAll("#tabla tr").forEach((fila, index) => {
    // Omitir la primera fila si es el encabezado
    if (index !== 0) {
        fila.addEventListener("mouseover", function() {
            this.style.backgroundColor = "#f2f2f2";
        });

        fila.addEventListener("mouseout", function() {
            this.style.backgroundColor = "";
        });
    }
});