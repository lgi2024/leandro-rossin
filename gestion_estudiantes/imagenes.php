<?php
function uploadImage($image) {
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($image["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    // Verificar si el archivo es una imagen real
    $check = getimagesize($image["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        return ["status" => 0, "message" => "El archivo no es una imagen."];
    }

    // Verificar si el archivo ya existe
    if (file_exists($target_file)) {
        return ["status" => 0, "message" => "Lo siento, el archivo ya existe."];
    }

    // Limitar el tamaño del archivo
    if ($image["size"] > 10000000) {
        return ["status" => 0, "message" => "Lo siento, tu archivo es demasiado grande."];
    }

    // Permitir ciertos formatos de imagen
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "jfif") {
        return ["status" => 0, "message" => "Lo siento, solo se permiten archivos JPG, JPEG, PNG."];
    }

    // Si todo está bien, intenta subir el archivo
    if ($uploadOk == 1) {
        if (move_uploaded_file($image["tmp_name"], $target_file)) {
            return ["status" => 1, "message" => "El archivo ". htmlspecialchars(basename($image["name"])). " ha sido subido.", "path" => $target_file];
        } else {
            return ["status" => 0, "message" => "Lo siento, hubo un error al subir tu archivo."];
        }
    }
}
?>