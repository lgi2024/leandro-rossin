<?php
include 'conexion.php';

/// Create database
$sql = "CREATE DATABASE escuela";
if (mysqli_query($conn, $sql)) {
  echo "Base creada correctamente";
} else {
  echo "Error creando la base: " . mysqli_error($conn);
}

mysqli_close($conn);
?>