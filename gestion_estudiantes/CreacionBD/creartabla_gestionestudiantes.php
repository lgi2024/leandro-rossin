<?php
include 'conexion.php';

// sql to create table
$sql = "CREATE TABLE estudiantes (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(30) NOT NULL,
edad int(3) NOT NULL,
email VARCHAR(50) NOT NULL,
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

if (mysqli_query($conn, $sql)) {
  echo "Tabla estudiantes creada correctamente";
} else {
  echo "Error creando tabla: " . mysqli_error($conn);
}

mysqli_close($conn);
?>
