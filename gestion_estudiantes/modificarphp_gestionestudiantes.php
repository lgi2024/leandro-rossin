<?php
include 'CreacionBD/conexion.php';

include 'imagenes.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $name = $_POST['nombre'];
    $age = $_POST['edad'];
    $email = $_POST['email'];

    $uploadResult = uploadImage($_FILES["foto"]);

    if ($uploadResult['status'] == 1) {
        $target_file = $uploadResult['path'];

        if ($age>=0){
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $sql = "UPDATE estudiantes SET foto='$target_file', nombre='$name', edad='$age', email='$email' WHERE id='$id'";

                if (mysqli_query($conn, $sql)) {
                    echo "<br>Datos actualizados con exito";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                }
            }else {
                echo "El correo electrónico no tiene un formato válido.";
            }
        }else {
            echo "La edad debe ser mayor a 0.";
        }
    }else {
        echo $uploadResult['message'];
    }
    mysqli_close($conn);
}
    

$id = $_GET['id'];


$sql = "SELECT id, nombre, edad, email FROM estudiantes WHERE id=$id";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
    $row = mysqli_fetch_assoc($result);
    $name = $row['nombre'];
    $age = $row['edad'];
    $email = $row['email'];
} else {
    echo "No se encontró el estudiante con ID: $id";
    exit();
}
mysqli_close($conn);
?>


<form method="post" action="" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $id; ?>">
    Nombre: <input type="text" name="nombre" value="<?php echo $name; ?>" required><br>
    Edad: <input type="number" name="edad" value="<?php echo $age; ?>" required><br>
    Correo electrónico: <input type="email" name="email" value="<?php echo $email; ?>" required><br>
    Seleccionar una imagen para cargar: <input type="file" name="foto" id="foto" required><br>
    <input type="submit" value="Actualizar">
    <a href='listaestudiantes.php'> Volver atras </a>
</form>




